<?php

namespace App\Traits;


trait ApiResponser {
	
	public function successResponse($data, $code)
    {
        return response()->json($data, $code);
    }


    /**
     * return error response.
     *
     * @return \Illuminate\Http\Response
     */
    public function errorResponse($error, $errorMessages = [], $code = 404)
    {
    	$response = [
            'success' => false,
            'message' => $error,
        ];


        if(!empty($errorMessages)){
            $response['data'] = $errorMessages;
        }


        return response()->json($response, $code);
    }

    public function showMessage($message, $code = 200)
    {
    	return $this->successResponse(['data' => $message], $code);
    }
}