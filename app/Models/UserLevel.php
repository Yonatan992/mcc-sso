<?php

namespace App\Models;

use App\Models\Level;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserLevel extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'user_levels';

    protected $fillable = [
        'user_id',
        'level_id',
        'phone',
        'email',
    ];


    //RelationShips
    public function user() {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function level() {
        return $this->belongsTo(Level::class, 'user_id');
    }
}
