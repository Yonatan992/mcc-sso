<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Level extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'nivel_numero' => $this->number,
            'detalle' => $this->description,
            'creacion' => $this->created_at->format('d/m/Y'),
            'modificacion' => $this->updated_at->format('d/m/Y'),
        ];
    }
}
