<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserLevel extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'user' => $this->user_id,
            'level' => $this->level_id,
            'phone' => $this->phone,
            'email' => $this->email,
            'created' => $this->created_at->format('d/m/Y'),
            'updated' => $this->updated_at->format('d/m/Y'),
        ];
    }
}
