<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\API\BaseController as BaseController;
use App\Http\Controllers\Controller;
use App\Models\User;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends BaseController
{
    public function __construct()
    {
        $this->middleware('client.credentials')->only(['register']);
        $this->middleware('auth:api')->except(['register']);
    }


    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email:rfc,dns',
            'password' => 'required',
            'confirm_password' => 'required|same:password',
        ]);
   
        if($validator->fails()){
            return $this->errorResponse('Validation Error.', $validator->errors());       
        }
   
        $input = $request->all();
        $input['password'] = bcrypt($input['password']);
        $input['verification_token'] = User::generarVerificationToken();
        $input['verified'] = false;
        $user = User::create($input);

        $success['name'] =  $user->name;
   
        return $this->successResponse(['data' => $success], 200);
    }
   
    /**
     * Login api
     *
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {
        if(Auth::attempt(['email' => $request->email, 'password' => $request->password])){ 
            $user = Auth::user(); 
            $success['token'] =  $user->createToken('MyApp')->accessToken; 
            $success['name'] =  $user->name;
   
            return $this->successResponse($success, 'User login successfully.');
        } 
        else{ 
            return $this->errorResponse('Unauthorized.', ['error'=>'Unauthorized']);
        }
    }


    public function verify($token) 
    {
        $user = User::where('verification_token', $token)->firstOrFail();

        $user->verified = true;
        $user->verification_token = null;

        $user->save();

        return $this->showMessage('La cuenta ha sido verificada');

    }
}
