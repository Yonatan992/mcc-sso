<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\API\BaseController as BaseController;
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;

class UserController extends BaseController
{
	public function __construct()
    {
        $this->middleware('client.credentials')->only(['index']);
    }

    public function index()
    {
    	$users = User::with('level')->get();
    	return $this->successResponse($users, 200);
    }

    public function show($id)
    {
    	$user = User::where('id', $id)->firstOrFail();
    	return $this->successResponse($user, 200);
    }

    public function destroy($id)
    {
    	$user = User::where('id', $id)->firstOrFail();
    	$user->delete();

    	return $this->showMessage('El usuario ha sido eliminado');
    }
}
