<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\API\BaseController as BaseController;
use App\Http\Controllers\Controller;
use App\Models\UserLevel;
use Illuminate\Http\Request;

class UserLevelController extends BaseController
{
    public function __construct()
    {
        $this->middleware('client.credentials')->only(['index']);
    }

    public function index()
    {
        $usersLevels = UserLevel::all();
    
        return $this->successResponse(UserLevelResource::collection($usersLevels), 'Data retrieved successfully.');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    
}
