<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\API\BaseController as BaseController;
use App\Http\Controllers\Controller;
use App\Http\Resources\Level as LevelResource;
use App\Models\Level;
use Illuminate\Http\Request;
use Validator;

class LevelController extends BaseController
{
    public function __construct()
    {
        $this->middleware('client.credentials')->only(['index', 'show']);
    }

    public function index()
    {
        $levels = Level::all();
    
        return $this->successResponse(LevelResource::collection($levels), 'Niveles obtenidos correctamente');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
   
        $validator = Validator::make($input, [
            'number' => 'required',
            'description' => 'required'
        ]);
   
        if($validator->fails()){
            return $this->errorResponse('Validation Error.', $validator->errors());       
        }
   
        $level = Level::create($input);
   
        return $this->successResponse(new LevelResource($level), 'Level created successfully.');
    } 
   
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $level = Level::findOrFail($id);
  
        if (is_null($level)) {
            return $this->errorResponse('Product not found.');
        }
   
        return $this->successResponse(new LevelResource($level), 'Product retrieved successfully.');
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Level $level)
    {
        $input = $request->all();
   
        $validator = Validator::make($input, [
            'number' => 'required',
            'description' => 'required'
        ]);
   
        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }
   
        $level->name = $input['number'];
        $level->detail = $input['description'];
        $level->save();
   
        return $this->sendResponse(new LevelResource($level), 'Level updated successfully.');
    }
   
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Level $level)
    {
        $level->delete();
   
        return $this->successResponse([], 'Level deleted successfully.');
    }
}
