Hola {{ $user->name }}

Gracias por registrarte, por favor verifica tu email ingresando al siguiente link:

{{ route('user.verify', ['token' => $user->verification_token]) }}
